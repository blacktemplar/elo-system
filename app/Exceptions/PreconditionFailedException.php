<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 1/13/18
 * Time: 12:13 PM
 */

namespace App\Exceptions;


/**
 * Class PreconditionFailedException
 * @package App\Exceptions
 */
class PreconditionFailedException extends AbstractException
{
}