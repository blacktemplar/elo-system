<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 9/22/17
 * Time: 6:52 PM
 */

namespace App\Exceptions;


/**
 * Class AbstractException
 * @package App\Exceptions
 */
class AbstractException extends \Exception
{

}