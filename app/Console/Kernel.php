<?php
declare(strict_types=1);

namespace App\Console;

use Laravel\Lumen\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 * @package App\Console
 */
class Kernel extends ConsoleKernel
{
//<editor-fold desc="Fields">
  /**
   * The Artisan commands provided by your application.
   *
   * @var array
   */
  protected $commands = [
    //
  ];
//</editor-fold desc="Fields">
}
