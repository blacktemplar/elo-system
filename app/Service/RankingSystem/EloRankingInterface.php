<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 3/2/17
 * Time: 4:29 PM
 */

namespace App\Service\RankingSystem;


/**
 * Interface EloRankingInterface
 * @package App\Service\TournamentRanking
 */
interface EloRankingInterface extends GameRankingSystemInterface
{

}