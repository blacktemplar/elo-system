<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 1/2/18
 * Time: 2:36 PM
 */

namespace App\Service\RankingSystem;


/**
 * Interface GameRankingSystemInterface
 * @package App\Service\RankingSystemListService
 */
interface GameRankingSystemInterface extends RankingSystemInterface
{

}