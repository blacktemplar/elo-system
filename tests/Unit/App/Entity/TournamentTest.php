<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/1/17
 * Time: 1:11 PM
 */

namespace Tests\Unit\App\Entity;

use App\Entity\Competition;
use App\Entity\Tournament;
use App\Entity\User;
use App\Helpers\Level;
use Doctrine\Common\Collections\ArrayCollection;
use Tests\Helpers\UnitTestCase;

/**
 * Class TournamentTest
 * @package Tests\Unit\App\Entity
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class TournamentTest extends UnitTestCase
{
//<editor-fold desc="Public Methods">
  /**
   * @covers \App\Entity\Tournament::getCompetitions
   * @covers \App\Entity\Tournament::getChildren
   * @uses   \App\Entity\Competition::__construct
   * @uses   \App\Entity\Helpers\NameEntity::getName
   * @uses   \App\Entity\Helpers\NameEntity::setName
   * @uses   \App\Entity\Tournament::__construct
   * @uses   \App\Entity\Helpers\TournamentHierarchyEntity::__construct
   */
  public function testCompetitionsAndChildren()
  {
    $tournament = $this->tournament();
    $competition = new Competition();
    $competition->setName('comp name');
    self::assertEquals($tournament->getCompetitions(), $tournament->getChildren());
    $tournament->getCompetitions()->set($competition->getName(), $competition);
    self::assertEquals(1, $tournament->getCompetitions()->count());
    self::assertEquals($competition, $tournament->getCompetitions()[$competition->getName()]);
    self::assertEquals($tournament->getCompetitions(), $tournament->getChildren());
  }

  /**
   * @covers \App\Entity\Tournament::__construct
   * @uses   \App\Entity\Tournament::getCompetitions
   * @uses   \App\Entity\Tournament::getTournamentListId
   * @uses   \App\Entity\Helpers\TournamentHierarchyEntity::__construct
   */
  public function testConstructor()
  {
    $tournament = $this->tournament();
    self::assertInstanceOf(Tournament::class, $tournament);
    self::assertInstanceOf(ArrayCollection::class, $tournament->getCompetitions());
    self::assertEquals(0, $tournament->getCompetitions()->count());
    self::assertEquals("", $tournament->getTournamentListId());
  }

  /**
   * @covers \App\Entity\Tournament::setCreator
   * @covers \App\Entity\Tournament::getCreator
   * @uses   \App\Entity\Tournament::__construct
   * @uses   \App\Entity\User::__construct
   * @uses   \App\Entity\Helpers\TournamentHierarchyEntity::__construct
   */
  public function testCreator()
  {
    $tournament = $this->tournament();
    $creator = new User();
    $tournament->setCreator($creator);
    self::assertEquals($creator, $tournament->getCreator());
  }

  /**
   * @covers \App\Entity\Tournament::getLocalIdentifier
   * @uses   \App\Entity\Helpers\UUIDEntity::getId
   * @uses   \App\Entity\Tournament::__construct
   * @uses   \App\Entity\Helpers\TournamentHierarchyEntity::__construct
   */
  public function testGetLocalIdentifier()
  {
    $tournament = $this->tournament();
    /** @noinspection PhpUnhandledExceptionInspection */
    self::getProperty(get_class($tournament), 'id')->setValue($tournament, 'user-id');
    self::assertEquals($tournament->getId(), $tournament->getLocalIdentifier());
  }

  /**
   * @covers \App\Entity\Tournament::getLevel
   * @uses   \App\Entity\Tournament::__construct
   * @uses   \App\Entity\Helpers\TournamentHierarchyEntity::__construct
   */
  public function testLevel()
  {
    self::assertEquals(Level::TOURNAMENT, $this->tournament()->getLevel());
  }

  /**
   * @covers \App\Entity\Tournament::getParent
   * @uses   \App\Entity\Tournament::__construct
   * @uses   \App\Entity\Helpers\TournamentHierarchyEntity::__construct
   */
  public function testParent()
  {
    self::assertNull($this->tournament()->getParent());
  }

  /**
   * @covers \App\Entity\Tournament::setTournamentListId
   * @covers \App\Entity\Tournament::getTournamentListId
   * @uses   \App\Entity\Tournament::__construct
   * @uses   \App\Entity\Helpers\TournamentHierarchyEntity::__construct
   */
  public function testTournamentListId()
  {
    $tournament = $this->tournament();
    $tournament->setTournamentListId("Changed");
    self::assertEquals("Changed", $tournament->getTournamentListId());
  }

  /**
   * @covers \App\Entity\Tournament::setUserIdentifier
   * @covers \App\Entity\Tournament::getUserIdentifier
   * @uses   \App\Entity\Tournament::__construct
   * @uses   \App\Entity\Helpers\TournamentHierarchyEntity::__construct
   */
  public function testUserIdentifier()
  {
    $tournament = $this->tournament();
    $tournament->setUserIdentifier("UserIdentifier");
    self::assertEquals("UserIdentifier", $tournament->getUserIdentifier());
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Private Methods">
  /**
   * @return Tournament a new tournament
   */
  private function tournament(): Tournament
  {
    return new Tournament();
  }
//</editor-fold desc="Private Methods">
}